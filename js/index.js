document.addEventListener("DOMContentLoaded", () => {
    ymaps.ready(init);

    function init(){ 
        var myMap = new ymaps.Map("map", {
            
            center: [55.76, 37.64],
            
            zoom: 7
        });
    };
    
    $("#education-select").focus();
    $(".burger-button").on("click", () => {
        $(".header__mobile-menu").toggleClass("header__mobile-menu_opened");
        $(".burger-button__line-1").toggleClass("burger-button__line-1_active");
        $(".burger-button__line-2").toggleClass("burger-button__line-2_active");
        $(".burger-button__line-3").toggleClass("burger-button__line-3_active");
    });
    
    $("#popup-form-cross").on("click", (EO) => {
        EO.preventDefault();
        $(".popup-form-container").css("display", "none");
    });
    
    $(".order-card-button, .schedule-button").on("click", (EO) => {
        EO.preventDefault();
        $(".popup-form-container").css("display", "flex");
        $("#education-select").focus();
    });
    
    $(".popup-form__form").on("submit", (EO) => {
        EO.preventDefault();
        $(".popup-form-container").css("display", "none");
        $(".message-box-container").css("display", "flex");
    });
    
    $("#message-box-cross").on("click", (EO) => {
        EO.preventDefault();
        $(".message-box-container").css("display", "none");
    });
    
    window.addEventListener("scroll", (EO) => {
        const top = this.pageYOffset;
        $(".parallax-spot-1").css("transform", 'translate3d(0px, ' + top*0.1 + 'px, 0px)');
        $(".parallax-spot-2").css("transform", 'translate3d(0px, ' + top*0.2 + 'px, 0px)');
    });
    
    const height = $(window).height();
    $('.scroll-button').click(function () {
        $('body,html').animate({
            scrollTop: height + 100
        }, 600);
        return false;
    });
});
